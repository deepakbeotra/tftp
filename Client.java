/**
 *
 * @Filename Client.java
 *
 * @Version $Id: Client.java,v 1.0 2014/09/25 05:15:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */


import java.io.FileOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * This class makes the connection with any TFTP server and downloads the file
 * by acting as a TFTP Client.
 * 
 * 
 * @author1 Deepak Mahajan
 * 
 */
public class Client {

	/**
	 * This is the get function. On given get (fileName) command, this function
	 * get called. It downloads the file if that file is available on TFTP
	 * server
	 * 
	 * @param IPAddress
	 *            .. to make connection to required inet address sc.. to take
	 *            user input for file name
	 * 
	 */

	public void get(InetAddress IPAddress, Scanner sc) {
		// Acknowledgment packet
		byte[] ackPacket = new byte[4];
		ackPacket[0] = (byte) 0x00;
		ackPacket[1] = (byte) 0x04;
		ackPacket[2] = (byte) -1;
		ackPacket[3] = (byte) -1;
		DatagramSocket clientSocket = null;
		int rPort = 69;
		boolean fileCreated = false;
		boolean timeOutCheck = false;
		try {
			clientSocket = new DatagramSocket();
			// setting time to time out
			// if server does not support tftp
			clientSocket.setSoTimeout(5000);
			byte[] receiveData = new byte[516];
			byte[] fileBuffer;
			System.out.print("(file) ");
			String fileName = sc.next();
			StringTokenizer parts = new StringTokenizer(fileName, "/");
			String destinationFile = null;
			while (parts.hasMoreTokens()) {
				destinationFile = parts.nextElement().toString();
			}

			byte[] file = fileName.getBytes();

			byte[] terminator = new byte[2];
			terminator[0] = (byte) 0x00;
			terminator[1] = (byte) 0x01;
			byte[] mode = "octet".getBytes();

			byte[] firstPacket = new byte[file.length + mode.length + 4];
			int j = 0;
			firstPacket[j] = terminator[0];
			j++;
			firstPacket[j] = terminator[1];
			j++;
			for (int i = 0; i < file.length; i++) {
				firstPacket[j] = file[i];
				j++;
			}
			firstPacket[j] = terminator[0];
			j++;
			for (int i = 0; i < mode.length; i++) {
				firstPacket[j] = mode[i];
				j++;
			}
			firstPacket[j] = terminator[0];

			DatagramPacket sendPacket = new DatagramPacket(firstPacket,
					firstPacket.length, IPAddress, rPort);
			clientSocket.send(sendPacket);
			FileOutputStream fos;
			DatagramPacket receivePacket = null;
			while (true) {
				if (!timeOutCheck) {
					timeOutCheck = true;
					try {
						receivePacket = new DatagramPacket(receiveData,
								receiveData.length);
						clientSocket.receive(receivePacket);

					} catch (SocketTimeoutException t) {
						System.out
								.println("Time Out... Try Again with another host");
						startCon(IPAddress, sc, false);
						break;

					}
				} else {
					clientSocket.setSoTimeout(35000);
					receivePacket = new DatagramPacket(receiveData,
							receiveData.length);
					clientSocket.receive(receivePacket);
				}

				if (receiveData[1] == 5 && receiveData[3] == 1) {
					System.out.println("File not found..");
					break;
				} else if (receiveData[1] == 5 && receiveData[3] == 2) {
					System.out.println("Access violation..");
					break;
				} else if (receiveData[1] == 5 && receiveData[3] == 4) {
					System.out.println("Illegal TFTP operation..");
					break;
				} else if (receiveData[1] == 5 && receiveData[3] == 5) {
					System.out.println("Unknwon transfer ID..");
					break;
				} else if (receiveData[1] == 3
						&& ((ackPacket[2] != receivePacket.getData()[2]) || (ackPacket[3] != receivePacket
								.getData()[3]))) {

					fileBuffer = new byte[receivePacket.getLength() - 4];
					for (int i = 0; i < fileBuffer.length; i++) {
						fileBuffer[i] = receiveData[4 + i];
					}
					if (!fileCreated) {
						fos = new FileOutputStream(destinationFile);
						fos.write(fileBuffer);
						fos.close();
						fileCreated = true;
					} else {
						fos = new FileOutputStream(destinationFile, true);
						fos.write(fileBuffer);
						fos.close();
					}
					if (receivePacket.getLength() < 516) {
						System.out.println("Done...");
						break;
					} else {

						ackPacket[2] = receivePacket.getData()[2];
						ackPacket[3] = receivePacket.getData()[3];
						rPort = receivePacket.getPort();
						DatagramPacket sendAckPacket = new DatagramPacket(
								ackPacket, ackPacket.length, IPAddress, rPort);
						clientSocket.send(sendAckPacket);
						receiveData = new byte[516];
						ackPacket = new byte[4];
						ackPacket[0] = (byte) 0x00;
						ackPacket[1] = (byte) 0x04;
					}
				} else if ((receiveData[1] == 3 && ackPacket[2] == receivePacket
						.getData()[2])
						&& (ackPacket[3] == receivePacket.getData()[3])) {
					// retransmission
					System.out.println("retransmission");
					DatagramPacket sendAckPacket = new DatagramPacket(
							ackPacket, ackPacket.length, IPAddress, rPort);
					clientSocket.send(sendAckPacket);
				}

			}
			clientSocket.close();
		}

		catch (Exception e) {

		}

	}

	/**
	 * This is the main function. It creates the Client object.
	 * 
	 * @param args
	 *            not used
	 */

	public static void main(String args[]) throws Exception {

		Scanner sc = new Scanner(System.in);
		boolean flag = false;
		InetAddress IPAddress = null;
		Client c = new Client();
		c.startCon(IPAddress, sc, flag);
	}

	/**
	 * This is the startCon function. It accepts the user commands which are
	 * required for TFTP protocol.
	 * 
	 * @param IPAddress
	 *            .. to make connection to required Inet address sc.. to take
	 *            user input for TFTP commands and flag to maitain a check of
	 *            whether is connection is succefully built or not.
	 */
	public void startCon(InetAddress IPAddress, Scanner sc, boolean flag) {
		while (true) {
			System.out.print("tftp*> ");

			String input = sc.next();
			if (input.equals("get") && flag) {
				get(IPAddress, sc);
			} else if (input.equals("get") && !flag) {
				System.out.println("Not connected..");
			}

			else if (input.equals("connect")) {
				System.out.print("(to) ");
				String ip = sc.next();
				try {
					IPAddress = InetAddress.getByName(ip);
					flag = true;
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					System.out.println(ip + ": unknown host");
					flag = false;
				}

			} else if (input.equals("quit")) {
				System.exit(0);
			} else {
				System.out.println("?Invalid command..");
			}
		}
	}
}
